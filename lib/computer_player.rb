class ComputerPlayer
  attr_accessor :board
  attr_reader :name, :ships

  def initialize(name)
    @name = name
    @ships = Ship.initial_ships
  end

  def get_play
    get_pos
  end

  def display(board, with_ships = false)
    @board = board
    @board.render(:with_ships => with_ships)
  end

  def get_start_pos
    get_pos
  end

  def get_bearing
    ['h', 'v'].sample
  end

  def get_y_or_n
    ['y', 'n'].sample
  end

  private

  def get_pos
    places = @board.positions_without([:x, :o])
    places.sample
  end
end
