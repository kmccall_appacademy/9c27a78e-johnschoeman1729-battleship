class Ship
  @@ships = {
    aircraftcarrier:  ['Aircraft Carrier', 5, nil, nil],
    battleship:       ['Battleship', 4, nil, nil],
    submarine:        ['Submarine', 3, nil, nil],
    destroyer:        ['Destroyer', 3, nil, nil],
    patrolboat:       ['Patrol Boat', 2, nil, nil]
  }

  attr_reader :name, :length, :bearing, :start_pos

  def initialize(name, length, bearing, start_pos)
    @name = name
    @length = length
    @bearing = bearing
    @start_pos = start_pos
  end

  def self.initial_ships
    player_ships = {}
    @@ships.each do |k, v|
      player_ships[k] = Ship.new(v[0], v[1], v[2], v[3])
    end
    player_ships
  end

end
