class HumanPlayer
  attr_reader :name
  attr_accessor :ships

  def initialize(name)
    @name = name
    @ships = Ship.initial_ships
  end

  def get_play
    get_pos("Enter where you'd like to attack: ")
  end

  def display(board, with_ships = false)
    board.render(:with_ships => with_ships)
  end

  def get_start_pos
    get_pos("Enter the start position: ")
  end

  def get_bearing
    print "Enter the ship's bearing ('h' or 'v'): "
    gets.chomp
  end

  def get_y_or_n
    gets.chomp.downcase
  end

  private

  def get_pos(text)
    loop do
      print text
      pos = gets.chomp.split(" ").map(&:to_i)
      if pos.length == 2
        return pos
      else
        puts "Not a valid position"
      end
    end
  end
end
