class Board
  attr_reader :grid
  attr_accessor :destroyed_ships

  def initialize(grid = nil)
    @grid = grid.nil? ? Board.default_grid : grid
    @grid.populate_grid_randomly if @grid.empty?
    @destroyed_ships = 0
  end

  def [](pos) # why no working?
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end

  def self.default_grid
    Array.new(10) { Array.new(10) { nil } }
  end

  def count
    @grid.flatten.compact.count(:s)
  end

  def won?
    return true if empty?
    false
  end

  def empty?
    count == 0
  end

  def has_ship?(pos)
    self[pos] == :s
  end

  def has_attack?(pos)
    self[pos] == :x || self[pos] == :o
  end

  def full?
    count == grid.flatten.size
  end

  def populate_grid_randomly(ships)
    loop do
      ship_key = ships.keys.sample
      ship = ships[ship_key]
      unless place_ship(ship.length, random_pos, random_bearing)
        next
      end
      ships.delete(ship_key)
      break if ships.empty?
    end
  end

  def place_ship(length, start_pos, bearing = 'horz')
    return false unless is_on_board?(length, start_pos, bearing)
    return false if crosses_a_ship?(length, start_pos, bearing)
    place_ship_with_bearing(length, start_pos, bearing)
  end

  def is_on_board?(length, start_pos, bearing = 'horz')
    return false unless in_range?(start_pos)
    if ['horz', 'h'].include?(bearing)
      return false if start_pos[1] + length > @grid[0].length
    elsif ['vert', 'v'].include?(bearing)
      return false if start_pos[0] + length > @grid.length
    end
    true
  end

  def crosses_a_ship?(length, start_pos, bearing = 'horz')
    row, col = start_pos
    if ['horz', 'h'].include?(bearing)
      @grid[row][col..col + length].include?(:s)
    elsif ['vert', 'v'].include?(bearing)
      @grid.transpose[col][row..row + length].include?(:s)
    end
  end

  def in_range?(pos)
    pos[0] <= (@grid[0].size - 1) && pos[1] <= (@grid.size - 1)
  end

  def render(options = {})
    defaults = {
      with_ships: false,
      grid: @grid
    }
    options = defaults.merge(options)
    with_ships = options[:with_ships]
    grid = options[:grid]
    grid = grid_without_ships(grid) unless with_ships
    puts board_to_string(grid)
  end

  def positions_without(symbols) #[:s, :o, :x]
    positions = []
    (0...@grid.length).each do |row|
      (0...@grid[0].length).each do |col|
        pos = self[[row, col]]
        positions << [row, col] if !symbols.include?(pos)
      end
    end
    positions
  end

  private

  def random_pos
    [rand(0..9), rand(0..9)]
  end

  def random_bearing
    ['horz', 'vert'].sample
  end

  def grid_without_ships(grid)
    grid.map do |row|
      row.map do |col|
        col == :s ? nil : col
      end
    end
  end

  def board_to_string(grid)
    boarder = "-" * (@grid[0].length * 3) + "\n"
    string = boarder
    grid.each do |row|
      line = ""
      row.each do |col|
        !col.nil? ? line << " #{col.to_s} " : line << " - "
      end
      string += line + "\n"
    end
    string += boarder
  end

  def place_ship_with_bearing(length, start_pos, bearing)
    if ['horz', 'h'].include?(bearing)
      row, col = start_pos
      while col - start_pos[1] < length
        self[[row, col]] = :s
        col += 1
      end
    elsif ['vert', 'v'].include?(bearing)
      row, col = start_pos
      while row - start_pos[0] < length
        self[[row, col]] = :s
        row += 1
      end
    end
    true
  end
end
