require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'
require_relative 'ship'

require 'byebug'

class BattleshipGame
  attr_reader :player1, :player2, :board1, :board2
  attr_accessor :current_player, :current_board

  def initialize(player1, player2 = nil)
    @player1 = player1
    @player2 = player2
    @current_player = player1

    @board1 = Board.new
    @board2 = Board.new
    @current_board = @board1
  end

  def play
    setup_game
    until game_over?
      switch_player!
      play_turn
    end
    conclude_game
  end

  def setup_game
    puts "Welcome to Battleship!"
    setup(@player1, @board2, @player1.ships)
    setup(@player2, @board1, @player2.ships)
  end

  def conclude_game
    puts "Game Over!"
    puts "#{@current_player.name} has won!"
    show_players_board
    switch_player!
    show_players_board
  end

  def show_players_board
    puts "#{@current_player.name}'s Board: "
    @current_board.render(:with_ships => true)
  end

  def setup(player, board, ships)
    start_setup(player)
    y_or_n = player.get_y_or_n
    if y_or_n == 'y'
      board.populate_grid_randomly(ships)
    else
      player_place_all_ships(player, board, ships)
    end
  end

  def start_setup(player)
    puts "#{player.name} it's your turn to setup."
    print "Would you like to randomly layout your ships? (y or n): "
  end

  def player_place_all_ships(player, board, ships)
    ships.each do |_, ship|
      player_place_ship(player, board, ship)
    end
  end

  def player_place_ship(player, board, ship)
    loop do
      start_pos, bearing = get_placement_info(player, board, ship)
      if !board.place_ship(ship.length, start_pos, bearing)
        puts "Not a vaild placement for your #{ship.name}"
      else
        break
      end
    end
  end

  def get_placement_info(player, board, ship)
    player.display(board, true)
    puts "#{player.name} chose where you'd like to place #{ship.name}: "
    [player.get_start_pos, player.get_bearing]
  end

  def play_turn
    start_turn
    pos, has_ship, has_attack = get_attack_choice
    return if handle_attack_choice(pos, has_ship, has_attack)
    finish_turn
  end

  def start_turn
    puts "#{@current_player.name} it's your turn!"
    display_status
    @current_player.display(@current_board)
  end

  def get_attack_choice
    pos = @current_player.get_play
    has_ship = @current_board.has_ship?(pos)
    has_attack = @current_board.has_attack?(pos)
    [pos, has_ship, has_attack] # [[0, 0], true, false]
  end

  def handle_attack_choice(pos, has_ship, has_attack)
    return if already_attacked(has_attack)
    attack(pos)
    board_has_ship(has_ship)
  end

  def finish_turn
    @current_player.display(@current_board)
  end

  def already_attacked(has_attack)
    if has_attack
      puts "You've already fired there!"
      switch_player!
      return true
    end
    false
  end

  def board_has_ship(has_ship)
    if has_ship
      puts "You hit a ship!"
    else
      puts "You missed!"
    end
  end

  def switch_player!
    if @current_player == @player1
      @current_player = @player2
      @current_board = @board2
    else
      @current_player = @player1
      @current_board = @board1
    end
  end

  def attack(pos)
    if @current_board.has_ship?(pos)
      @current_board.destroyed_ships += 1
      @current_board[pos] = :x
    else
      @current_board[pos] = :o
    end
  end

  def count
    @current_board.count
  end

  def game_over?
    @current_board.won?
  end

  def display_status
    puts "Remaining Ships: #{@current_board.count}"
    puts "Destroyed Ships: #{@current_board.destroyed_ships}"
  end
end

if __FILE__ == $PROGRAM_NAME
  human1 = HumanPlayer.new("FinTheHuman")
  human2 = HumanPlayer.new("JakeTheDog")
  computer1 = ComputerPlayer.new("TotallyNotARobot")
  computer2 = ComputerPlayer.new("AlsoNotARobot")
  game = BattleshipGame.new(computer1, computer2)
  game.play
end
