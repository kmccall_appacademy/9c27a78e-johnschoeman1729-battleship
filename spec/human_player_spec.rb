require "rspec"
require "battleship"

describe "HumanPlayer" do
  let(:human) { HumanPlayer.new("Human") }
  let(:human2) { HumanPlayer.new("Human2")}
  let(:board) { Board.new }
  let(:game)  { BattleshipGame.new(human, human2) }
  let(:ships) { Ship.initial_ships }

  class NoMoreInput < StandardError
  end

  before do
    $stdout = StringIO.new
    $stdin = StringIO.new

    class HumanPlayer
      def gets
        result = $stdin.gets
        raise NoMoreInput unless result

        result
      end
    end

    def recent_output
      outputs = $stdout.string.split("\n")
      max = [outputs.length, 5].min
      outputs[-max..-1].join(" ")
    end

    def human.get_play!
      get_play
      rescue NoMoreInput
    end

    board.populate_grid_randomly(ships)
  end

  describe "#initialize" do
    it "initializes with a name" do
      expect(human.name).to eq("Human")
    end
  end

  describe "#get_play" do
    it "asks for a play" do
      human.get_play!
      expect($stdout.string.downcase).to match(/where/)
    end

    it "should take an entry of the form '0 0' and return a position" do
      $stdin.string << "0 0"

      expect(human.get_play).to eq([0, 0])
    end
  end

  describe "#display" do
    it "prints the board to the screen" do
      game.attack([0, 0])
      human.display(game.board1)

      expect($stdout.string).to match(/[ox]/)
    end

    it "doesn't show where hidden ships are" do
      game.board1.populate_grid_randomly(Ship.initial_ships)
      human.display(game.board1)

      expect($stdout.string).not_to match(/s/)
    end
  end

end
