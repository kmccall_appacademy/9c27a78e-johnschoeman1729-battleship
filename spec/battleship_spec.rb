require "rspec"
require "battleship"

describe BattleshipGame do
  let(:two_ship_grid) { [[:s, :s], [nil, nil]] }
  let(:two_ship_board1) { Board.new(two_ship_grid) }

  let(:two_ship_board2) { Board.new(two_ship_grid) }

  let(:player1) { double("player1") }
  let(:player2) { double("player2") }

  let(:game) { BattleshipGame.new(player1, player2) }

  describe "#initialize" do

    context "only player is an arguement" do
      it "accepts only a player as an arguement" do
        expect { BattleshipGame.new(player1) }.not_to raise_error
      end
    end

    context "only players as arguements" do
      it "accepts two players as arguements" do
        expect { BattleshipGame.new(player1, player2) }.not_to raise_error
      end
    end
  end

  describe "#attack" do
    it "marks the board1 at the specified position" do
      game.attack([1, 1])

      expect(game.current_board[[1, 1]]).not_to eql(nil)
    end

  end

  describe "#count" do
    it "delegates to the board1's #count method" do
      expect(game.board1).to receive(:count)

      game.count
    end
  end

  describe "#game_over?" do
    it "delegates to the board1's #won? method" do
      expect(game.board1).to receive(:won?)

      game.game_over?
    end
  end

  describe "#play_turn" do
    it "gets a move from the player and makes an attack at that position" do
      allow(player1).to receive(:get_play).and_return([1, 1])
      allow(player1).to receive(:name).and_return("Player1")
      allow(player1).to receive(:display).and_return(:two_ship_board)

      expect(player1).to receive(:get_play)
      expect(game).to receive(:attack).with([1, 1])

      game.play_turn
    end

    it "doesn't let a player attack the same spot twice" do
      allow(player1).to receive(:get_play).and_return([1, 1])
      allow(player1).to receive(:name).and_return("Player1")
      allow(player1).to receive(:display).and_return(:two_ship_board)

      expect(player1).to receive(:get_play)
      2.times { expect(game).to receive(:attack).with([1, 1]) }

      game.play_turn
      game.play_turn
    end
  end

  describe "#setup" do
    it "setups up the board" do
    end

    it "doesn't let a player place a ship in a place that already has a ship" do
    end

  end

  describe "#play" do
    it "plays the game" do

    end
  end

  describe "#display_status" do
    it "displays the status" do

    end
  end

  describe "#switch_player!" do
    it "switches the players" do
      first_player = game.current_player
      game.switch_player!
      second_player = game.current_player
      expect(first_player).not_to eql(second_player)
      game.switch_player!
      third_player = game.current_player
      expect(third_player).to eql(first_player)
    end
  end
end
