require "rspec"
require "battleship"

describe ComputerPlayer do
  let(:computer) { ComputerPlayer.new("Computer") }
  let(:board) { Board.new }
  let(:game)  { BattleshipGame.new(computer, board) }

  describe "#initialize" do
    it "initializes with a name" do
      expect(computer.name).to eq("Computer")
    end
  end

  describe "#get_play" do
    it "returns a position that is in range" do
      
    end

    it "should take an entry of the form '0 0' and return a position" do

      # expect(coumputer.get_play).to eq([0, 0])
    end
  end

  describe "#display" do
    it "prints the board to the screen" do
      game.attack([0, 0])
      computer.display(board)

      # expect( ).to
    end

    it "doesn't show where hidden ships are" do
      computer.display(board)

      #  expect().not_to match(/s/)
    end
  end
end
