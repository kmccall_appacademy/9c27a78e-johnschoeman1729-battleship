require "rspec"
require "battleship"

describe Ship do
  let(:ship) { Ship.new('Patrol Boat', 2, 'horz', [1, 1]) }

  describe "#initialize" do
    it "initializes with name, length, bearing and start_pos" do
      expect(ship.name).to eql('Patrol Boat')
      expect(ship.length).to eql(2)
      expect(ship.bearing).to eql('horz')
      expect(ship.start_pos).to eql([1, 1])
    end
  end
end
