require "rspec"
require "battleship"

describe Board do
  subject(:board) { Board.new }

  let(:empty_grid) { Array.new(10) { Array.new(10) } }
  let(:empty_board) { Board.new(empty_grid) }

  let(:two_empty_grid) { Array.new(2) { Array.new(2) } }
  let(:two_empty_board) { Board.new(two_empty_grid) }

  let(:two_ship_grid) { [[:s, :s], [nil, nil]] }
  let(:two_ship_board) { Board.new(two_ship_grid) }

  let(:full_grid) { [[:s, :s], [:s, :s]] }
  let(:full_board) { Board.new(full_grid) }


  let(:attacked_grid) { [[:x, :s, :o], [:o, nil, :x], [:s, :o, nil]] }
  let(:attacked_board) { Board.new(attacked_grid) }

  before do
    $stdout = StringIO.new

    def recent_output
      outputs = $stdout.string.split("\n")
      max = [outputs.length, 5].min
      outputs[-max..-1].join(" ")
    end

  end

  describe "::default_grid" do
    let(:grid) { Board.default_grid }

    it "returns a 10x10 grid" do
      expect(grid.length).to eq(10)

      grid.each do |row|
        expect(row.length).to eq(10)
      end
    end
  end

  describe "#initialize" do
    context "when passed a grid" do
      it "initializes with the provided grid" do
        expect(empty_board.grid).to eq(empty_grid)
      end
    end

    context "when not passed a grid" do
      it "initializes with Board::default_grid" do
        grid = Board.default_grid

        expect(Board).to receive(:default_grid).and_call_original

        expect(board.grid).to eq(grid)
      end
    end
  end

  describe "#count" do
    it "returns the number of ships on the board" do
      expect(empty_board.count).to eq(0)
      expect(two_ship_board.count).to eq(2)
    end
  end

  describe "#empty?" do

    context "when not passed a position" do
      context "with ships on the board" do
        it "returns false" do
          expect(two_ship_board).not_to be_empty
        end
      end

      context "with no ships on the board" do
        it "returns true" do
          expect(empty_board).to be_empty
        end
      end
    end
  end

  describe "#has_ship?" do
    context "when position has a ship" do
      it "returns true" do
        expect(two_ship_board.has_ship?([0, 0])).to be_truthy
      end
    end

    context "when position doesn't have a ship" do
      it "returns false" do
        expect(two_ship_board.has_ship?([1, 0])).to be_falsey
      end
    end
  end

  describe "#full?" do
    context "when the board is full" do
      it "returns true" do
        expect(full_board).to be_full
      end
    end

    context "when the board is not full" do
      it "returns false" do
        expect(two_ship_board).not_to be_full
      end
    end
  end

  describe "#won?" do
    context "when no ships remain" do
      it "returns true" do
        expect(empty_board).to be_won
      end
    end

    context "when at least one ship remains" do
      it "returns false" do
        expect(two_ship_board).not_to be_won
      end
    end
  end

  describe "#populate_grid_randomly" do
    it "adds the correct number of ships to board" do
      board = empty_board.dup
      ships = Ship.initial_ships
      board.populate_grid_randomly(ships)
      expect(board.count).to eql(17)
    end
  end

  describe "#place_ship" do
    it "places a ship horizontaily" do
      board = two_empty_board.dup
      board.place_ship(2, [0, 0], 'horz')
      expect(board.grid).to eql(two_ship_board.grid)
    end

    it "places a ship verticaly" do
      board = two_empty_board.dup
      board.place_ship(2, [0, 0], 'vert')
      expect(board.grid).to eql([[:s, nil], [:s, nil]])
    end

    it "places a larger ship" do
      board = empty_board.dup
      board.place_ship(5, [1, 1], 'horz')
      board2 = empty_board.dup
      board2.grid[1][1..5] = [:s, :s, :s, :s, :s]
      expect(board.grid).to eql(board2.grid)
    end
  end

  describe "#is_on_board?" do
    it "returns false if trying to place a ship off the board" do
      expect(empty_board.is_on_board?(5, [0, 9], 'horz')).to be_falsey
    end

    it "returns true if not trying to place a ship off the board" do
      expect(empty_board.is_on_board?(5, [0, 0], 'horz')).to be_truthy
    end

    it "works with vertical ships too" do
      expect(empty_board.is_on_board?(5, [9, 0], 'vert')).to be_falsey
      expect(empty_board.is_on_board?(5, [0, 9], 'vert')).to be_truthy
    end
  end

  describe "#crosses_a_ship" do
    it "returns true if placing a ship crosses an existing ship" do
      board = empty_board.dup
      board.place_ship(5, [1, 1], 'horz')
      expect(board.crosses_a_ship?(5, [0, 1], 'vert')).to be_truthy
    end

    it "returns false if placing a ship doesnt cross an exiting ship" do
      board = empty_board.dup
      board.place_ship(5, [1, 1], 'vert')
      expect(board.crosses_a_ship?(5, [1, 2], 'horz')).to be_falsey
    end
  end

  describe "#in_range?" do
    context "out of range" do
      it "returns false" do
        expect(two_ship_board.in_range?([3, 3])).to be_falsey
      end
    end

    context "in range" do
      it "returns true" do
        expect(two_ship_board.in_range?([1, 1])).to be_truthy
      end
    end
  end

  describe "#render" do
    it "prints the board" do
      two_ship_board.render
      expect($stdout.string).not_to be_empty
    end

    it "prints the board with ships" do
      two_ship_board.render(:with_ships => true)
      expect($stdout.string).to match(/s/)
    end

    it "prints the board without ships" do
      two_ship_board.render(:with_ships => false)
      expect($stdout.string).not_to match(/s/)
    end
  end

  describe "#positions_without"

  context "when given :x and :o" do
    it "returns all positions that don't have an :x or :o" do
      expect(attacked_board.positions_without([:x, :o])).to eql([[0, 1], [1, 1], [2, 0], [2, 2]])
    end
  end

  context "when given :x :s :o" do
    it "returns all positions that are nil" do
      expect(two_empty_board.positions_without([:x, :s, :o])).to eq([[0, 0], [0, 1], [1, 0], [1, 1]])
      expect(two_ship_board.positions_without([:x, :s, :o])).to eq([[1, 0], [1, 1]])
      expect(full_board.positions_without([:x, :s, :o])).to eq([])
    end
  end
end
